from playbook_runner import AnsiblePlaybookRunner

if __name__ == "__main__":
    print("[INPUT]")
    bot_count = int(input("bot count: "))

    runner = AnsiblePlaybookRunner(
        name = "joyboy", 
        bot_count = bot_count
    )

    # runner.run_create_nodepool()
    # runner.run_create_deployment()

    # input("Node Pool and Deployment created.\nPress Enter to trigger workers...")

    # workers_ip = runner.get_worker_ip_address()
    # workers_ip = ",".join(workers_ip)
    # master_pod_name = runner.get_master_pod_name()
    # runner.run_trigger_workers(master_pod_name, workers_ip)

    # input("Worker triggered.\nPress Enter to delete...")

    runner.run_delete_deployment()
    runner.run_delete_nodepool()

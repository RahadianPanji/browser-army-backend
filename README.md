# browser-army

This 

## Installation

Since we use ansible, a linux runtime is recommended.
Dependencies:
- Python >= 3.8.10
    - For the python packages, see `requirements.txt`. You can install all by running `pip install -r requirements.txt`.
- Ansible modules
    - To enable creating GKE Node Pools, `ansible-galaxy install google.cloud`

## Usage
The kubernetes cluster is already created and ready to use, hosted on Google Kubernetes Engine. This ansible scripts are for automating creation and deletion of node pool, deployment, and selenium script execution on each worker.

### Creating Node Pool
```
ansible-playbook ./playbooks/create-nodepool.yml
```

### Creating Deployment
```
ansible-playbook ./playbooks/create-deployment.yml
```
Each replica of deployments will be in the form of a pod. 

Kubernetes will automatically assign each pod to a node. Typically, if each node has the same machine type (same power), it will assign each node a same number of pod **(THIS NEEDS PROOFING, also for multiple node pool with differenct machine type)**.

### Prepare to Execute Selenium Script
For each master and worker pod, we use the process created here https://gitlab.com/aulia-adil/kp-master-process.

It first requires the IP Address of each worker, for the master to send the signal to run the script. Master will send the signal once it receives the trigger from User.

#### Collect the Workers' IP Address (WIP)
```
ansible-playbook ./playbooks/get-worker-ip.yml
```

### Execute Senelium Script (WIP)

### Stopping the Deployment
```
ansible-playbook ./playbooks/delete-deployment.yml
```

### Destroying Node Pool
```
ansible-playbook ./playbooks/delete-nodepool.yml
```

import ansible_runner
import os

class AnsiblePlaybookRunner:
    def __init__(self, name, bot_count, 
                zone="us-central1-c", 
                machine_type="n1-standard-1", 
                node_count=None):

        if not node_count:
            node_count = max(1, bot_count//5) # TODO: experiment on this
        if node_count > bot_count:
            node_count = bot_count

        self.params = {
            'name': name,
            'machine_type': machine_type,
            'zone': zone,
            'bot_count': bot_count,
            'node_count': node_count,
        }

    def run_create_nodepool(self):
        print(f"Creating Node Pool with variables: {self.params}")

        r = ansible_runner.run(playbook="/root/browser-army/playbooks/create-nodepool.yml", envvars=self.params)
        
        if r.status != 'successful':
            raise Exception("Playbook create-nodepool didn't run successfully")
        
        created_node_pool = r.get_fact_cache("localhost")['created_node_pool']
        return created_node_pool

    def run_delete_nodepool(self):
        print(f"Deleting Node Pool with variables: {self.params}")

        r = ansible_runner.run(playbook="/root/browser-army/playbooks/delete-nodepool.yml", envvars=self.params)
        
        if r.status != 'successful':
            raise Exception("Playbook delete-nodepool didn't run successfully")
        
        deleted_node_pool = r.get_fact_cache("localhost")['deleted_node_pool']
        return deleted_node_pool

    def run_create_deployment(self):
        print(f"Creating deployment with variables: {self.params}")

        # refers to this https://stackoverflow.com/questions/77392152/return-integer-value-without-quote-when-using-variable
        os.environ["ANSIBLE_JINJA2_NATIVE"] = "true"
        r = ansible_runner.run(playbook="/root/browser-army/playbooks/create-deployment.yml", envvars=self.params)
        del os.environ['ANSIBLE_JINJA2_NATIVE']
        
        if r.status != 'successful':
            raise Exception("Playbook create-deployment didn't run successfully")
        
        created_deployment = r.get_fact_cache("localhost")['created_deployment']
        return created_deployment
        
    def run_delete_deployment(self):
        r = ansible_runner.run(playbook="/root/browser-army/playbooks/delete-deployment.yml", envvars=self.params)
        
        if r.status != 'successful':
            raise Exception("Playbook delete-deployment didn't run successfully")
        
        deleted_deployment = r.get_fact_cache("localhost")['deleted_deployment']
        return deleted_deployment
        
    def run_trigger_workers(self, master_pod_name, workers_ip):
        self.params['master_pod_name'] = master_pod_name
        self.params['workers_ip'] = workers_ip

        r = ansible_runner.run(playbook="/root/browser-army/playbooks/trigger-workers.yml", envvars=self.params)
        
        if r.status != 'successful':
            raise Exception("Playbook trigger-workers didn't run successfully")
        
        return

    def run_get_worker_pods(self):
        r = ansible_runner.run(playbook="/root/browser-army/playbooks/get-worker-pods.yml")

        if r.status != 'successful':
            raise Exception("Playbook get-worker-ip didn't run successfully")

        pods_info = r.get_fact_cache("localhost")['pods_info']
        return pods_info

    def run_get_master_pod(self):
        r = ansible_runner.run(playbook="/root/browser-army/playbooks/get-master-pod.yml")

        if r.status != 'successful':
            raise Exception("Playbook get-master-pod didn't run successfully")

        pods_info = r.get_fact_cache("localhost")['pods_info']
        return pods_info

    def get_master_pod_name(self):
        pods = self.run_get_master_pod()
        return pods[0]['metadata']['name']

    def get_worker_ip_address(self):
        pods = self.run_get_worker_pods()
        return [ pod['status']['podIP'] for pod in pods ]
